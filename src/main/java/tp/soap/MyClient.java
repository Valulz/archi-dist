package tp.soap;

import tp.model.City;
import tp.model.CityManagerService;
import tp.model.CityNotFound;
import tp.model.Position;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class MyClient {
    private static final QName SERVICE_NAME = new QName("http://model.tp/", "CityManagerService");
    private static final QName PORT_NAME = new QName("http://model.tp/", "CityManagerPort");

    public static void main(String[] args) throws MalformedURLException, CityNotFound {
        URL wsdlURL = new URL("http://tomcat-valulz.rhcloud.com/citymanager-1.1.6/citymanager?wsdl");
        Service service = Service.create(wsdlURL, SERVICE_NAME);
        CityManagerService cityManager = service.getPort(PORT_NAME, CityManagerService.class);

//        System.out.println(cityManager.getCities());
//        City c = new City("Zanarkand", 16, 64, "Hyrule");
//        System.out.println(cityManager.addCity(c));
//        System.out.println(cityManager.getCities());
//        System.out.println(cityManager.searchExactPosition(new Position(16, 64)));
//        System.out.println(cityManager.addCity(new City("Zan", 15.9, 64, "Hy"));
//        System.out.println(cityManager.searchFor("Zan"));
//        System.out.println(cityManager.searchNear(new Position(15.95, 64)));
//        System.out.println(cityManager.removeCity(c));
//        System.out.println(cityManager.getCities());
//        cityManager.removeCities();
//        System.out.println(cityManager.getCities());


        System.out.println("\nAfficher l'ensemble des villes");
        System.out.println(cityManager.getCities());

        System.out.println("\nSupprimer toutes les villes");
        cityManager.removeCities();

        System.out.println("\nAfficher l'ensemble des villes");
        System.out.println(cityManager.getCities());

        System.out.println("\nAjouter Rouen en France");
        System.out.println(cityManager.addCity(new City("Rouen", 49.443889, 1.103333, "France")));

        System.out.println("\nAjouter Mogadiscio en Somalie");
        System.out.println(cityManager.addCity(new City("Mogadiscio", 2.333333, 48.85, "Somalie")));

        System.out.println("\nAjouter Rouen en France");
        System.out.println(cityManager.addCity(new City("Rouen", 49.443889, 1.103333, "France")));

        System.out.println("\nAjouter Bihorel en France");
        System.out.println(cityManager.addCity(new City("Bihorel", 49.455278, 1.116944, "France")));

        System.out.println("\nAjouter Londres en Angleterre");
        System.out.println(cityManager.addCity(new City("Londres", 51.504872, -0.07857, "Angleterre")));

        System.out.println("\nAjouter Paris en France");
        System.out.println(cityManager.addCity(new City("Paris", 48.856578, 2.351828, "France")));

        System.out.println("\nAjouter Paris en Canada");
        System.out.println(cityManager.addCity(new City("Paris", 43.2, -80.38333, "Canada")));

        System.out.println("\nAfficher l'ensemble des villes");
        System.out.println(cityManager.getCities());

        System.out.println("\nAjouter Villers-Bocage en France");
        System.out.println(cityManager.addCity(new City("Villers-Bocage", 49.083333, -0.65, "France")));

        System.out.println("\nAjouter Villers-Bocage en France");
        System.out.println(cityManager.addCity(new City("Villers-Bocage", 50.021858, 2.326126, "France")));

        System.out.println("\nAfficher l'ensemble des villes");
        System.out.println(cityManager.getCities());

        System.out.println("\nSupprimer Villers-Bocage en France (Latitude : 49.856578, Longitude : -0.65)");
        System.out.println(cityManager.removeCity(new City("Villers-Bocage", 49.083333, -0.65, "France")));

        System.out.println("\nAfficher l'ensemble des villes");
        System.out.println(cityManager.getCities());

        System.out.println("\nSupprimer Londres en Angleterre");
        System.out.println(cityManager.removeCity(new City("Londres", 51.504872, -0.07857, "Angleterre")));

        System.out.println("\nSupprimer Londres en Angleterre");
        System.out.println(cityManager.removeCity(new City("Londres", 51.504872, -0.07857, "Angleterre")));

        System.out.println("\nAfficher la ville située à la position exacte (49.443889, 1.103333)");
        try {
            System.out.println(cityManager.searchExactPosition(new Position(49.443889, 1.103333)));
        } catch (CityNotFound ex){
            System.out.println("Normalement ça devrait pas arriver !!!!");
        }

        System.out.println("\nAfficher la ville située à la position exacte (49.083333, -0.65)");
        try{
            System.out.println(cityManager.searchExactPosition(new Position(49.083333, -0.65)));
        } catch (CityNotFound ex){
            System.out.println("Exception catché !!!!!");
        }

        System.out.println("\nAfficher la ville située à la position exacte (43.2, -80.38)");
        try{
            System.out.println(cityManager.searchExactPosition(new Position(43.2, -80.38)));
        }catch (CityNotFound ex){
            System.out.println("Exception catché !!!!!");
        }

        System.out.println("\nAfficher les villes situées à 10km de la position (48.85, 2.34)");
        System.out.println(cityManager.searchNear(new Position(48.85, 2.34)));

        System.out.println("\nAfficher les villes situées à 10km de la position (42, 64)");
        System.out.println(cityManager.searchNear(new Position(42, 64)));

        System.out.println("\nAfficher les villes situées à 10km de la position (49.45, 1.11)");
        System.out.println(cityManager.searchNear(new Position(49.45, 1.11)));

        System.out.println("\nAfficher la(les) ville(s) nommée(s) \"Mogadiscio\"");
        System.out.println(cityManager.searchFor("Mogadiscio"));

        System.out.println("\nAfficher la(les) ville(s) nommée(s) \"Paris\"");
        System.out.println(cityManager.searchFor("Paris"));

        System.out.println("\nAfficher la(les) ville(s) nommée(s) \"Hyrule\"");
        System.out.println(cityManager.searchFor("Hyrule"));

        System.out.println("\nSupprimer toues les villes");
//        cityManager.removeCities();

        System.out.println("\nAfficher l'ensemble des villes");
        System.out.println(cityManager.getCities());



    }

}

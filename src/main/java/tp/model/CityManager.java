package tp.model;

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.geojson.Point;
import org.bson.Document;

import javax.jws.WebService;
import java.util.*;

/**
 * This class represent a city manager, it can
 * <ul>
 * <li>add a city</li>
 * <li>remove a city</li>
 * <li>return the list of cities</li>
 * <li>search a city with a given name</li>
 * <li>search a city at a position</li>
 * <li>return the list of cities near 10km of the given position</li>
 * </ul>
 */
@WebService(endpointInterface = "tp.model.CityManagerService", serviceName = "CityManagerService")
public class CityManager implements CityManagerService {

    private static final Map<String, String> env = System.getenv();

    private static final String DB_HOST = env.get("OPENSHIFT_MONGODB_DB_HOST");
    private static final String DB_PORT = env.get("OPENSHIFT_MONGODB_DB_PORT");
    private static final String DB_NAME = env.get("OPENSHIFT_MONGODB_DB_USERNAME");
    private static final String DB_PASS = env.get("OPENSHIFT_MONGODB_DB_PASSWORD");
    private static final String CITY_TABLE = "cities";

    /**
     * Converts a City to a DBObject (for Database operations)
     * @param city the city to be converted
     * @return a dbObject of the city
     */
    private DBObject cityToDBObj(City city){
        return new BasicDBObject("name", city.getName())
                .append("latitude", city.getPosition().getLatitude())
                .append("longitude", city.getPosition().getLongitude())
                .append("country", city.getCountry());

    }

    /**
     * Converts a DBObject to a City
     * @param object the dbObject to be converted
     * @return a city of the DBObject
     */
    private City dbObjToCity(DBObject object){
        City city = new City();
        city.setName((String) object.get("name"));
        city.setCountry((String)object.get("country"));
        city.setPosition(new Position((Double)object.get("latitude"), (Double) object.get("longitude")));

        return city;
    }

    /**
     * Constant used in the method searchNear().
     * The number of kilometers around which we are looking for city.
     */
    private static final int NEAR_KM = 10;

    /**
     * Constant used in the method searchNear().
     * Represents the Earth radius.
     */
    private static final double EARTH = 6374892.5;

    private List<City> cities;
    private DBCollection citiesDB;

    /**
     * Constructor of CityManager
     */
    public CityManager() {
        this.cities = new LinkedList<>();
        MongoCredential credential = MongoCredential.createCredential(DB_NAME, "tomcat", DB_PASS.toCharArray());
        MongoClient client = new MongoClient(new ServerAddress(DB_HOST, Integer.parseInt(DB_PORT)),
                Arrays.asList(credential));
        DB db = client.getDB("tomcat");

        citiesDB = db.getCollection("cities");

    }


    public List<City> getCities() {
        List<City> cities = new ArrayList<>();
        DBCursor cursor = citiesDB.find();

        while(cursor.hasNext()){
            cities.add(dbObjToCity(cursor.next()));
        }

        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    /**
     * Appends the specified city to the end of the list
     *
     * @param city the city to add at the end of the list
     * @return true if the city has been added
     */
    public boolean addCity(City city) {
        citiesDB.insert(cityToDBObj(city));
        return true;
    }

    /**
     * Remove the first occurrence of the specified city from this list, if it is presents.
     *
     * @param city the city to be removed.
     * @return true if the city has been removed.
     */
    public boolean removeCity(City city) {
        citiesDB.remove(cityToDBObj(city));
        return true;
    }

    /**
     * Remove all the cities from the list.
     */
    public void removeCities(){
        citiesDB.remove(new BasicDBObject());
    }

    /**
     * Search and return the cities that match the specified name
     *
     * @param cityName the name of the city/cities searches
     * @return The list of the cities found
     */
    public List<City> searchFor(String cityName) {
        List<City> citiesSearched = new ArrayList<City>();

        DBObject query = new BasicDBObject("name",
                new BasicDBObject("$regex", ".*"+cityName+".*"));
        DBCursor cursor = citiesDB.find(query);

        try {
            while(cursor.hasNext()) {
                citiesSearched.add(dbObjToCity(cursor.next()));
            }
        } finally {
            cursor.close();
        }

        return citiesSearched;
    }

    /**
     * Search for the city at the position specified.
     *
     * @param position position of the searched city
     * @return the city found
     * @throws CityNotFound if no city is found at the specified position
     */
    public City searchExactPosition(Position position) throws CityNotFound {

        DBObject query = new BasicDBObject("latitude", position.getLatitude())
                .append("longitude", position.getLongitude());

        DBCursor cursor = citiesDB.find(query);


        if(cursor.count() >= 1){
            City city = dbObjToCity(cursor.next());
            cursor.close();
            return city;
        }else {
            cursor.close();
            throw new CityNotFound();
        }

    }

    /**
     * Search cities around 10 kilometers from the specified position
     *
     * @param p the position searched
     * @return the list of city found
     */
    public List<City> searchNear(Position p) {
        //TODO
        List<City> cities = getCities();
		/*pLat and pLon are respectively the latitude and longitude of p convert in radiant*/
        double pLat = Math.toRadians(p.getLatitude()), pLon = Math.toRadians(p.getLongitude());

        double oLat, oLon;

        double tmp;

        List<City> citiesFound = new ArrayList<City>();

        for (City c : cities) {
            oLat = Math.toRadians(c.getPosition().getLatitude());
            oLon = Math.toRadians(c.getPosition().getLongitude());

            tmp = Math.acos(
                    Math.sin(pLat)
                            * Math.sin(oLat)
                            + Math.cos(pLat)
                            * Math.cos(oLat)
                            * Math.cos(oLon - pLon)
            );

			/*this instruction convert the distance into meters (by multiply it with the earth radius)
            * then by dividing it by 1000 into kilometers*/
            tmp = tmp * EARTH / 1000;

            if (tmp <= NEAR_KM) {
                citiesFound.add(c);
            }
        }

        return citiesFound;
    }
}
